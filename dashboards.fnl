#!/usr/bin/env fennel
(local yaml (require :ryaml))
(local ryaml yaml)

(local utils (require :utils))
(local pp utils.pp)
(local dbg utils.dbg)

(local icons (require :icons))
(local users (require :users))
(local wallpanel (require :wallpanel))

(local (cards shroom)
       (values (. (require :cards) :cards) (. (require :cards) :shroom)))

(local templates {})

(fn templates.ishome [yah nah user]
  (table.concat ["{% if is_state(\""
                 user.entity
                 "\", \"home\" ) -%} "
                 yah
                 " {%- else -%} "
                 nah
                 " {%- endif %}"]))

;;			{:type "light"
;;			 :entity "light.mainlamp"
;;			 :name "anotherOne"}

(fn view [arg]
  ;;settings headder cards
  (local data {:theme (or arg.settings.theme :palenight)
               :title arg.settings.title
               :type (or arg.settings.type "custom:vertical-layout")
               ;;(or settings.type "panel")
               "max_cols:" 1
               :width (or arg.settings.width 300)
               :column_widths "90%"
               ;;		  :cards cards
               })
  (local allcards (icollect [i v (ipairs arg.headder)]
                    v))
  (each [key value (ipairs arg.cards)]
    (table.insert allcards value))
  (tset data :cards allcards)
  ;;(collect [k v (pairs moresettings)])
  data)

(local modules
       {:header [(cards.shroom.title (shroom.style {:title "Hello, {{ user }}!"}))
                 (cards.shroom.chips {:chips [{:type :menu}
                                              {:type :entity
                                               :entity :device_tracker.phone}]
                                      :alignment :center})]
        :mainlamp {:shroom []
                   ;;:default
                   }
        :userinfo (fn [user]
                    (local states user.preferences.states)
                    (local msg user.preferences.states.message)
                    (local usericons user.preferences.states.icon)
                    (local userbadge user.preferences.states.badge)
                    (cards.shroom.templatecard
					 (shroom.helpers.templatecardhelper
					  {:primary
					   (templates.ishome
						msg.home.primary
                        msg.away.primary
                        user)
                       :secondary
					   (templates.ishome
						msg.home.secondary
                        msg.away.secondary
                        user)}
                      {:icon
					   (templates.ishome
						usericons.home.icon
                        usericons.away.icon
                        user)
                       :colour
					   (templates.ishome
						usericons.home.colour
                        usericons.away.colour
                        user)}
                      {:badge
					   (templates.ishome
						userbadge.home.badge
                        userbadge.away.badge
                        user)
                       :colour
					   (templates.ishome
						userbadge.home.colour
                        userbadge.away.colour
                        user)}
                      user.entity
                      :vertical)))})

(local views {:desktop
			  (view
			   {:settings
				{:title :desktop-panel
                 :type "custom:vertical-layout"
                 :type "custom:vertical-layout"
                 ;;:width 500
                 ;;:subview true
                 ;;:path ""
                 }
                :headder modules.header
                :cards
				[;;
                 (cards.stacks.horizontal
				  [(cards.shroom.light
					(shroom.helpers.lighthelper
					 {:lightcol true}
                     {:temperature true
                      :brightness true
                      :colour true}
                     :light.mainlamp
                     :vertical))])
                 (cards.stacks.horizontal [(modules.userinfo users.erik)])
                 ;;(cards.stacks.horizontal [])
                 ;;footer
                 ]})
              :mobile {}})

(local structure {:views [views.desktop]})

;;(setmetatable structure.views ryaml.array_mt)
;;(setmetatable  ryaml.array_mt)
(print (ryaml.encode structure))

;;modules

