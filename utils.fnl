(local fennel (require :fennel))

(local utils
	   {
		:dodebug false
		})


(fn utils.pp [obj]
	(print (fennel.view obj))
	)

(fn utils.dbg [obj]
  (when utils.dodebug
	(when obj.dbg
	  (print (.. "#### #### " obj.dbg.section " #### ####"))
	  )

	(utils.pp obj)
	(print "#### #### section-end #### ####")
	)
  )


utils
