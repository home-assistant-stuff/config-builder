#!/usr/bin/env fennel
(local utils (require :utils))
(local pp utils.pp)
(local dbg utils.dbg)

(local icons (require :icons))
(local users (require :users))

(local
 (cards shroom)
 (values
  (. (require :cards) :cards)
  (. (require :cards) :shroom)))


(fn [modules]
{
 :enabled true
 :debug false
 :hide_toolbar false
 :hide_sidebar false
 :fullscreen false
 :idle_time 10
 :keep_screen_on_time 86400
 :black_screen_after_time 7200
 :control_reactivation_time 1.0
 ;;:screensaver_stop_navigation_path /lovelace/default_view
 :image_url ""
 :image_fit "cover" ;"contain" or "fill"
 :image_list_update_interval 3600
 :image_order "sorted"
 :image_excludes []
 :show_exif_info false
 :fetch_address_data true
 :screensaver_entity "input_boolean.wallpanel_screensaver"
 :info_animation_duration_x 30
 :info_animation_duration_y 11
 :info_animation_timing_function_x "ease-in-out"
 :info_animation_timing_function_y "ease-in-out"
 :info_move_pattern "random"
 :info_move_interval 0
 :info_move_fade_duration 2.0
 :style {
		 :wallpanel-screensaver-info-box
		 {
		  :font-size "8vh"
		  :font-weight 600
		  :color "#eeeeee"
		  :text-shadow "-2px -2px 0 #03a9f4, 2px -2px 0 #03a9f4, -2px 2px 0 #03a9f4, 2px 2px 0 #03a9f4"
		  :wallpanel-screensaver-info-box
		  {
		   :--wp-card-width "450px"
		   }
		  :wallpanel-screensaver-info-box-content
		  {
		   :--ha-card-background "none"
		   :--ha-card-box-shadow "none"
		   }
		  :text-shadow "-0.5px -0.5px 0 rgb(17, 17, 17), 0.5px -0.5px 0 rgb(17, 17, 17), -0.5px 0.5px 0 rgb(17, 17, 17), 0.5px 0.5px 0 rgb(17, 17, 17)"
		  :--primary-text-color "#ffffff"
		  :--secondary-text-color "#dddddd"
		  }
		 }
 :cards
 [
   (modules.userinfo users.erik)
   ]
 :profiles
 {
  :black
  {:black_screen_after_time 2}
  :user.lena
  {:enabled false}
  }
 :profile "black"
 }
)
