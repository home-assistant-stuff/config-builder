#!/usr/bin/env fennel
(local utils (require :utils))
(local (pp dbg) (values utils.pp utils.dbg))
(local ryaml (require :ryaml))
(local yaml ryaml)



(fn card [cardType data]
  (local card {:type cardType})
  (when (not= data.cards nil)
    (setmetatable data.cards ryaml.array_mt))
  (each [k v (pairs data)]
    (tset card k v))
  card)

(fn entitycard [cardtype]
  (fn [entity] (card cardtype {: entity})))

(fn mkarray [data]
  (local out {})
  ;;(setmetatable out ryaml.array_mt)
  (each [k v (pairs data)]
    (tset out k v))
  ;;(tset out :level lvl)
  out)

(local helpers {})

(fn helpers.actions [acts]
  ;;tap hold dtap
  (local actions {})
  (if acts.tap (tset actions :tap_action acts.tap)
      acts.hold (tset actions :hold_action acts.hold)
      acts.dtap (tset actions :double_tap_action acts.dtap))
  ;; :tap_action
  ;; {:action tap}
  ;; :hold_action
  ;; {:action: hold}
  ;; :double_tap_action
  ;; {:action dtap}
  actions)

(local shroom {:helpers {} :presets {}})

(fn shroom.style [data]
  (local style {:alignment :center})
  (each [k v (pairs data)]
    (tset style k v))
  style)

(fn shroom.helpers.templatecardhelper [text icon badge entity layout]
  (fn mdi [ico]
    (.. "mdi:" ico))

  {:primary text.primary
   :secondary text.secondary
   :icon (mdi icon.icon)
   :icon_color icon.colour
   :badge_icon (mdi badge.badge)
   :badge_color badge.colour
   : entity
   : layout})

(fn shroom.helpers.lighthelper [style controls entity layout]
  (local light {:fill_container (or style.fill false)
                :use_light_color (or style.lightcol false)
                :show_color_temp_control (or controls.temperature false)
                :show_color_control (or controls.colour false)
                :show_brightness_control (or controls.brightness false)
                :collapsible_controls (or controls.collapsible false)
                : entity
                : layout})
  light)

;;type: conditional
;;conditions:
;;  - entity: input_boolean.justatest
;;    state: 'on'
;;card:

(fn condition [entity state])

(local cards {:basic {:light (entitycard :light)
                      :button (entitycard :button)
                      :entity (entitycard :entity)
                      :entities (fn [entities] (card :entities {: entities}))}
              :advanced {:conditional (fn [conditions card]
                                        (card :conditional
                                              {: conditions : card}))
                         :swipe (fn [cards]
                                  (card "custom:swipe-card" (mkarray {: cards})))}
              :stacks {:horizontal (fn [cards]
                                     (card :horizontal-stack
                                           (mkarray {: cards})))
                       :vertical (fn [cards]
                                   (card :vertical-stack (mkarray {: cards})))
                       :in {:horizontal (fn [cards]
                                          (card "custom:stack-in-card"
                                                (mkarray {: cards
                                                          :mode :horizontal})))
                            :vertical (fn [cards]
                                        (card "custom:stack-in-card"
                                              (mkarray {: cards
                                                        :mode :vertical})))}}
              :shroom {:title (fn [style]
                                (card "custom:mushroom-title-card"
                                      (shroom.style style)))
                       :templatecard (fn [style]
                                       (card "custom:mushroom-template-card"
                                             (shroom.style style)))
                       :chips (fn [style]
                                (card "custom:mushroom-chips-card"
                                      (shroom.style style)))
                       :light (fn [style]
                                (card "custom:mushroom-light-card"
                                      (shroom.style style)))}})

{: cards : shroom}

