#!/usr/bin/env fennel

{:phone
 {:battery
  {
   :level "sensor.phone_battery_level"
   :health "sensor.phone_battery_health"
   :power "sensor.phone_battery_power"
   :state "sensor.phone_battery_state"
   :temperature "sensor.phone_battery_temperature"
   }}}
