#!/usr/bin/env fennel
(local utils (require :utils))
(local pp utils.pp)
(local dbg utils.dbg)


(local icons (require :icons))
(local user {})
(set user.preferences {})
(set user.preferences.states {})
(set user.preferences.states.message {})
(set user.preferences.states.icon {})
(set user.preferences.states.badge {})


(local states user)
(set states.message {})
(set states.message.primary {})
(set states.message.secondary {})
(set states.icon {})
(set states.badge {})

(set states.message.primary.home {})
(set states.message.secondary.home {})
(set states.icon.home {})
(set states.badge.home {})

(set states.message.primary.away {})
(set states.message.secondary.away {})
(set states.icon.away {})
(set states.badge.away {})


(fn new [userdata]
  (local newuser user)
  ;;(dbg {: newuser :dbg {:section "before"}})
  (each [k v (pairs userdata)]
	(tset newuser k v)
	)
  ;;(dbg {: newuser :dbg {:section "after"}})
  newuser
  )

(local users
	   {
		:erik
		(new
		 {:name "Erik Lundstedt"
		  :nickname "erik"
		  :id "erik_lundstedt"
		  :entity "person.erik_lundstedt"
		  :preferences {}
		  }
		 )
		}
	   )



(set users.erik.preferences
	 {
	  :states
	  {:message
	   {:home {:primary  "Hello {{user}}."    :secondary "Welcome ~ , {{user}}."}
		:away {:primary  "{{user}} is away."  :secondary "We miss you {{user}}."}}
	   :icon
	   {:home {:icon icons.home-account :colour "blue" }
		:away {:icon icons.home :colour "blue"}}
	   :badge
	   {:home {:badge icons.linux   :colour "green"}
		:away {:badge icons.account :colour "red" }}
	   }
	  }
	 )

(dbg {:states users.erik.preferences.states})



users
